#pragma once
#include "table.h"

class Shop :public table, public Bask {
private:
	int money;
public:
	Shop();
	~Shop() {};
	void menu();
	void buy();
	int find(string s);
}; 

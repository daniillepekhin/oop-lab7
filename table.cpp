﻿#include "table.h"

//Таблица
void table::create_table(const char *s) {
	ifstream fi(s);
	if (!fi.is_open()) {
		cout << "Could not open the file or it does not exist\n";
		return;
	}
	string buff, b1, b2;
	getline(fi, buff, '\n');
	size_c = w_count(buff);
	while (!fi.eof()) {
		getline(fi, buff, '\n');
		this->size_r++;
	}
	fi.close();
	fi.open(s);
	string::size_type end;
	while ((b1 = fi.get()) == " ") {}
	getline(fi, buff, ' ');
	buff = b1 + buff;
	this->name2.push_back(buff);
	getline(fi, buff, '\n');
	this->name2.push_back(buff);
	this->arr = new int*[size_r];
	for (int i = 0; i < size_r; i++) {
		this->arr[i] = new int[size_c];
		getline(fi, buff, ' ');
		this->name1.push_back(buff);
		getline(fi, buff, '\n');
		for (int j = 0; j < size_c; j++) {
			this->arr[i][j] = stoi(buff, &end);
			buff = buff.substr(end);
		}
	}
	fi.close();
}

void table::show() {
	int n = 25;
	cout << "  _____ _    _  ____  _____ \n";
	cout << " / ____| |  | |/ __ \\|  __ \\ \n";
	cout << "| (___ | |__| | |  | | |__) | \n";
	cout << " \\___ \\|  __  | |  | |  ___/ \n";
	cout << " ____) | |  | | |__| | |\n";
	cout << "|_____/|_|  |_|\\____/|_|\n";
	cout << "Delicious Shawa\n\n";

	for (int i = -1; i < size_r; i++) {
		if (i == -1) {
			cout << setw(n) << left << "Any Shawa on your taste";
		}
		else {
			if (i > -1)
				cout << setw(n) << left << this->name1[i];
		}
		for (int j = 0; j < size_c; j++) {
			if (i == -1) {
				cout << setw(n) << right << this->name2[j];
			}
			else {
				cout << setw(n) << right << this->arr[i][j];
			}
		}
		cout << "\n";
	}
}

int table::w_count(string str) {
	int count = 0;
	int slovo = 0;
	int i = 0;
	while (str[i] == ' ' && str[i] != '\0')
		i++;
	while (str[i] != '\0') {
		if (str[i] != ' ' && slovo == 0) {
			slovo = 1;
			count++;
		}
		else if (str[i] == ' ')
			slovo = 0;
		i++;
	}
	return count;
}

//Корзина
Bask::Bask()
{
	this->size = this->sum = 0;
	this->size = this->summ = 0;
	this->size = this->sale = 0;
	this->begin = NULL;
}

void  Bask::push(string s, int n, int k)
{
	if (this->begin == NULL) {
		this->begin = new Node;
		this->begin->name = s;
		this->begin->count = n;
		this->begin->price = k;
		this->end = this->begin;
		this->sum += n * k;
		this->sale += (n * k) * 0.5;
		this->summ += (n * k) - sale;
		this->size = 1;
	}
	else {
		Node *tmp;
		tmp = new Node;
		tmp->name = s;
		tmp->count = n;
		tmp->price = k;
		tmp->prev = this->end;
		this->end->next = tmp;
		this->end = tmp;
		this->sum += n * k;
		this->sale += (n * k) * 0.5;
		this->summ += (n * k) - sale;
		this->size++;

	}
}

void Bask::print()
{
	int n = 25;
	cout << "Basket:\n";
	if (this->size == 0) {
		cout << "The basket is empty\n";
	}
	else {
		Node *tmp;
		cout << setw(n) << left << "Any Shawarma to your taste"
			<< setw(n) << right << "Number"
			<< setw(n) << right << "Price";
		cout << endl;
		tmp = this->begin;
		do {
			cout << setw(n) << left << tmp->name
				<< setw(n) << right << tmp->count
				<< setw(n) << right << tmp->price;
			cout << endl;
			tmp = tmp->next;
		} while (tmp != NULL);
		cout << "\nSumma pokupki: " << this->sum << " Ruble" << endl;
	}
	cout << endl;
}

void Bask::pop()
{
	if (this->size <= 1)
		this->clear();
	else {
		Node *tmp;
		tmp = this->end;
		this->sum -= tmp->count * tmp->price;
		this->summ -= tmp->count * tmp->price;   ////
		this->end = this->end->prev;
		this->end->next = NULL;
		this->size--;
	}
}

void Bask::clear() //Очистка корзины после покупки
{
	this->size = 0;
	this->sum = 0;
	Node *tmp;
	while (this->begin != this->end) {
		tmp = this->begin;
		this->begin = this->begin->next;
		delete tmp;
	}
	delete this->end;
	this->begin = this->end = NULL;
}
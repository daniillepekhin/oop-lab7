#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

using namespace std;

class table {
protected:
	int **arr;
	vector<string> name2, name1;
	int size_r = 0, size_c = 0;
public:
	table() {};
	~table() {};
	void create_table(const char *s);
	void show();
	int w_count(string str);
};

class Node {
public:
	string name;
	int count;
	int price;
	Node *next;
	Node *prev;
	Node() { prev = next = NULL; };
};

class List {
protected:
	Node *begin;
	Node *end;
public:
	virtual void push(string s, int n, int k) = 0;
	virtual void pop() = 0;
	virtual void print() = 0;
};

class Bask : public List {
protected:
	int size;
	int sum;
	int summ;
	int sale;
public:
	Bask();
	~Bask() {};
	void push(string s, int n, int k);
	void pop();
	void clear();
	void print();
};